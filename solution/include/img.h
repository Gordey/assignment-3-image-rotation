#ifndef _IMG_H_
#define _IMG_H_

#include <stdint.h>
#include <stdlib.h>

struct pixel {
    uint8_t b;
    uint8_t g;
    uint8_t r;
} __attribute__((packed));

struct image {
    size_t width;
    size_t height;
    struct pixel *data;
};

struct image image_init(size_t width, size_t height);
void image_free(struct image *restrict image);

struct pixel *image_at(const struct image *restrict image, size_t x, size_t y);

#endif
