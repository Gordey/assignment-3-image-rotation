#ifndef _IO_UTILS_H_
#define _IO_UTILS_H_

#include <stdbool.h>
#include <stdio.h>

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERROR
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

typedef struct {
    FILE *f;
} byte_istream;

typedef struct {
    FILE *f;
} byte_ostream;

byte_istream byte_istream_open(const char *restrict filename);
byte_ostream byte_ostream_open(const char *restrict filename);

enum read_status byte_istream_read(
    byte_istream in, void *buffer, size_t size, size_t elements, size_t padding
);

enum write_status byte_ostream_write(
    byte_ostream out, void *buffer, size_t size, size_t elements, size_t padding
);

void file_seek_set(FILE *f, size_t where);

#endif
