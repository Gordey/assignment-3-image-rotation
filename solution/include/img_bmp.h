#ifndef _BMP_H_
#define _BMP_H_

#include <stdio.h>

#include "img.h"
#include "io_utils.h"

struct bmp_file_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
} __attribute__((packed));

struct bmp_info_header {
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} __attribute__((packed)) ;

enum {
    BI_RGB = 0,
    BI_PELS_PER_METER = 0,
    BI_USED_ALL = 0,
    BI_IMPORTANT_ALL = 0,
    BF_TYPE_BM = 19778
};

struct bmp_header {
    struct bmp_file_header bf;
    struct bmp_info_header bi;
} __attribute__((packed)) ;

inline size_t row_size_with_padding(size_t row_size) {
    return ((row_size + 3) / 4) * 4;
}

inline size_t padding_size(size_t row_size) {
    return row_size_with_padding(row_size) - row_size;
}

enum read_status image_from_bmp(struct image *restrict image, byte_istream in);
enum write_status image_to_bmp(const struct image *restrict image, byte_ostream out);

#endif
