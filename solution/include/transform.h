#ifndef _TRANSFORM_H_
#define _TRANSFORM_H_

#include "img.h"

struct image image_rotate(struct image image);

#endif
