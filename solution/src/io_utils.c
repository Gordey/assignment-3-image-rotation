#include "io_utils.h"

#include <stdint.h>

byte_istream byte_istream_open(const char *restrict filename) {
    return (byte_istream) { fopen(filename, "rb") };
}

byte_ostream byte_ostream_open(const char *restrict filename) {
    return (byte_ostream) { fopen(filename, "wb") };
}

enum read_status byte_istream_read(
    byte_istream in, void *buffer, size_t size, size_t elements, size_t padding
) {
    size_t read_count = fread(buffer, size, elements, in.f);
    if (read_count != elements) {
        return READ_ERROR;
    }

    fseek(in.f, (long) padding, SEEK_CUR);
    return READ_OK;
}

enum write_status byte_ostream_write(
    byte_ostream out, void *buffer, size_t size, size_t elements, size_t padding
) {
    size_t write_count = fwrite(buffer, size, elements, out.f);
    if (write_count != elements) {
        return WRITE_ERROR;
    }

	while (padding--) {
    	fwrite(buffer, 1, 1, out.f);
	}

    return WRITE_OK;
}

void file_seek_set(FILE *f, size_t where) {
    fseek(f, (long) where, SEEK_SET);
}
