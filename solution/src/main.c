#include <stdio.h>
#include <stdlib.h>

#include "img_bmp.h"
#include "io_utils.h"
#include "transform.h"

const char *messages[] = {
	[READ_INVALID_HEADER] = "not a valid bmp file",
	[READ_INVALID_SIGNATURE] = "this bmp format is not supported",
	[READ_INVALID_BITS] = "image is corrupted"
};

int main(int argc, char **argv) {
    if (argc != 3) {
        fprintf(stderr, "usage: %s [src] [dest]\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    char *src_file = argv[1];
    byte_istream in = byte_istream_open(src_file);
    struct image src;

	enum read_status read_status = image_from_bmp(&src, in);
	if (read_status != READ_OK) {
		fprintf(stderr, "%s\n", messages[read_status]);
		exit(EXIT_FAILURE);
	}

    fclose(in.f);

    char *dest_file = argv[2];
    struct image dest = image_rotate(src);

    image_free(&src);

    byte_ostream out = byte_ostream_open(dest_file);

	int exit_code = EXIT_SUCCESS;

    if (image_to_bmp(&dest, out) != WRITE_OK) {
		fprintf(stderr, "unknown write error\n");
		exit_code = EXIT_FAILURE;
    }

	image_free(&dest);
	fclose(out.f);

    return exit_code;
}
