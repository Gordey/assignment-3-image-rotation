#include "img_bmp.h"
#include "io_utils.h"

#include <inttypes.h>
#include <stdint.h>
#include <string.h>

struct bmp_header bmp_header_for(const struct image *restrict image) {
    uint16_t bfType = BF_TYPE_BM;
    uint32_t bOffBits = sizeof(struct bmp_header);
    uint32_t bfReserved = 0;
    uint32_t biSizeImage = (row_size_with_padding(image->width * sizeof(struct pixel)) * image->height);

    return (struct bmp_header) {
        .bf = {
            .bfType = bfType,
            .bfileSize = bOffBits + biSizeImage,
            .bfReserved = bfReserved,
            .bOffBits = bOffBits
        },
        .bi = {
            .biSize = sizeof(struct bmp_info_header),
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = 1,
            .biBitCount = sizeof(struct pixel) * 8,
            .biCompression = BI_RGB,
            .biSizeImage = biSizeImage,
            .biXPelsPerMeter = BI_PELS_PER_METER,
            .biYPelsPerMeter = BI_PELS_PER_METER,
            .biClrUsed = BI_USED_ALL,
            .biClrImportant = BI_IMPORTANT_ALL
        }
    };
}

enum write_status image_to_bmp(const struct image *restrict image, byte_ostream out) {
    struct bmp_header header = bmp_header_for(image);
    if (byte_ostream_write(out, &header, sizeof(struct bmp_header), 1, 0) != WRITE_OK) {
        return WRITE_ERROR;
    }

    size_t padding = padding_size(image->width * sizeof(struct pixel));

    for (size_t i = 0; i < image->height; ++i) {
        struct pixel *row = image->data + (i * image->width);

        if (byte_ostream_write(out, row, sizeof(struct pixel), image->width, padding) != WRITE_OK) {
            return WRITE_ERROR;
        }
    }

    return WRITE_OK;
}
