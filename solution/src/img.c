#include "img.h"

#include <stdio.h>

struct image image_init(size_t width, size_t height) {
    struct image result = (struct image) {
        .width = width,
        .height = height,
        .data = malloc(width * height * sizeof(struct pixel))
    };

	if (result.data == NULL) {
		perror("Error");
		exit(EXIT_FAILURE);
	}

	return result;
}

void image_free(struct image *restrict image) {
    free(image->data);
}

struct pixel *image_at(const struct image *restrict image, size_t x, size_t y) {
    return &image->data[x + y * image->width];
}
