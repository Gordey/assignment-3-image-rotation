#include "img_bmp.h"
#include "io_utils.h"

#include <inttypes.h>
#include <stdint.h>
#include <string.h>

extern inline size_t row_size_with_padding(size_t row_size);

extern inline size_t padding_size(size_t row_size);

static enum read_status bmp_header_read(struct bmp_header *restrict header, byte_istream in) {
    if (byte_istream_read(in, &header->bf, sizeof(struct bmp_file_header), 1, 0) != READ_OK) {
        return READ_INVALID_HEADER;
    }

    if (header->bf.bfType != BF_TYPE_BM) {
        return READ_INVALID_SIGNATURE;
    }

    if (byte_istream_read(in, &header->bi, sizeof(struct bmp_info_header), 1, 0) != READ_OK) {
        return READ_INVALID_HEADER;
    }

    file_seek_set(in.f, header->bf.bOffBits);

    return READ_OK;
}

enum read_status image_from_bmp(struct image *restrict image, byte_istream in) {
    struct bmp_header header;
    enum read_status header_read_status = bmp_header_read(&header, in);

    if (header_read_status != READ_OK) {
        return header_read_status;
    }

    *image = image_init(header.bi.biWidth, header.bi.biHeight);

    size_t padding = padding_size(image->width * sizeof(struct pixel));

    for (size_t i = 0; i < image->height; ++i) {
        struct pixel *row = image->data + (i * image->width);
        if (byte_istream_read(in, row, sizeof(struct pixel), image->width, padding) != READ_OK) {
            return READ_INVALID_BITS;
        }
    }

    return READ_OK;
}
