#include "transform.h"

struct image image_rotate(struct image image) {
    struct image rotated_image = image_init(image.height, image.width);

    for (size_t y = 0; y < image.height; ++y) {
        for (size_t x = 0; x < image.width; ++x) {
            struct pixel source_color = *image_at(&image, x, y);
            *image_at(&rotated_image, image.height - y - 1, x) = source_color;
        }
    }

    return rotated_image;
}
